<?php
namespace App\Http\Controllers;

use App\Services\Finance;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function show()
    {
      return view('index');
    }

    public function companies()
    {
      $financeProvider = new Finance;
      return response()->json($financeProvider->getCompanies());
    }

    public function quote($code)
    {
      $financeProvider = new Finance;
      return response($financeProvider->getQuote($code));
    }

    public function directors($code)
    {
      $financeProvider = new Finance;
      return response()->json($financeProvider->getDirectors($code));
    }

    public function saveCompany(Request $request)
    {
      $company = new \App\Company;
      $company->name = $request->input('name');
      $company->currency = $request->input('currency');
      $company->save();
    }

    public function savedCompanies()
    {
      $rows = \App\Company::all()->sortByDesc('id')->take(5);

      $output = [];
      foreach ($rows as $row) {
        $output[] = $row->name;
      }
      
      return response()->json($output);
    }
}
